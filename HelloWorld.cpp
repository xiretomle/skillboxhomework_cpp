﻿#include <iostream>
#include <string>

class Animal {
private:
	std::string name;
public:
	Animal(std::string name) {
		this->name = name;
	}

	virtual void voice() {
		std::cout << name << " say: ";
	}
};

class Dog : public Animal {
public:
	Dog(std::string name) : Animal(name) {}

	virtual void voice() override {
		Animal::voice();
		std::cout << "Woof";
	}
};

class Cat : public Animal {
public:
	Cat(std::string name) : Animal(name) {}

	virtual void voice() override {
		Animal::voice();
		std::cout << "Meow";
	}
};

class Fish : public Animal {
public:
	Fish(std::string name) : Animal(name) {}

	virtual void voice() override {
		Animal::voice();
		std::cout << "Pop pop pop";
	}
};

class Cow : public Animal {
public:
	Cow(std::string name) : Animal(name) {}

	virtual void voice() override {
		Animal::voice();
		std::cout << "Mooooo";
	}
};

int main() {
	Animal* animals[] = {new Dog("Druzhok"), new Cat("Barsik"), new Cow("Masha"), new Fish("Gold")};

	for (int i = 0; i < 4; i++) {
		animals[i]->voice();
		std::cout << std::endl;
	}

	getchar();
}